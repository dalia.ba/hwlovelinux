## 11 Nov
El comando lshw es para saber la información del hardware
Cuando en la terminal ponemos "lshw -class disk" es para saber información del disco 
...
[root@a29 ~]# lshw -class disk
  *-disk                    
       description: ATA Disk
       product: ST500DM002-1BD14
       physical id: 0.0.0
       bus info: scsi@0:0.0.0
       logical name: /dev/sda
       version: KC48
       serial: Z6E4C0H8
       size: 465GiB (500GB)
       capabilities: partitioned partitioned:dos
       configuration: ansiversion=5 logicalsectorsize=512 sectorsize=4096 signature=000cb0b7

Comado para saber información de la memoria 
[root@a29 ~]# dmidecode -t memory
dmidecode 3.2
Getting SMBIOS data from sysfs.
SMBIOS 2.7 present.

Handle 0x0007, DMI type 16, 23 bytes
Physical Memory Array
	Location: System Board Or Motherboard
	Use: System Memory
	Error Correction Type: None
	Maximum Capacity: 32 GB
	Error Information Handle: Not Provided
	Number Of Devices: 2

Handle 0x0041, DMI type 17, 34 bytes
Memory Device
	Array Handle: 0x0007
	Error Information Handle: Not Provided
	Total Width: 64 bits
	Data Width: 64 bits
	Size: 4 GB
	Form Factor: DIMM
	Set: None
	Locator: ChannelA-DIMM0
	Bank Locator: BANK 0
	Type: DDR3
	Type Detail: Synchronous
	Speed: 1400 MT/s
	Manufacturer: Kingston
	Serial Number: 44567419
	Asset Tag: 9876543210
	Part Number: 9905584-014.A00LF 
	Rank: 1
	Configured Memory Speed: 1400 MT/s

Handle 0x0043, DMI type 17, 34 bytes
Memory Device
	Array Handle: 0x0007
	Error Information Handle: Not Provided
	Total Width: Unknown
	Data Width: Unknown
	Size: No Module Installed
	Form Factor: DIMM
	Set: None
	Locator: ChannelA-DIMM1
	Bank Locator: BANK 1
	Type: Unknown
	Type Detail: None
	Speed: Unknown
	Manufacturer: [Empty]
	Serial Number: [Empty]
	Asset Tag: 9876543210
	Part Number: [Empty]
	Rank: Unknown
	Configured Memory Speed: Unknown

Handle 0x0044, DMI type 17, 34 bytes
Memory Device
	Array Handle: 0x0007
	Error Information Handle: Not Provided
	Total Width: 64 bits
	Data Width: 64 bits
	Size: 4 GB
	Form Factor: DIMM
	Set: None
	Locator: ChannelB-DIMM0
	Bank Locator: BANK 2
	Type: DDR3
	Type Detail: Synchronous
	Speed: 1400 MT/s
	Manufacturer: Kingston
	Serial Number: 55561522
	Asset Tag: 9876543210
	Part Number: 9905584-014.A00LF 
	Rank: 1
	Configured Memory Speed: 1400 MT/s

Handle 0x0046, DMI type 17, 34 bytes
Memory Device
	Array Handle: 0x0007
	Error Information Handle: Not Provided
	Total Width: Unknown
	Data Width: Unknown
	Size: No Module Installed
	Form Factor: DIMM
	Set: None
	Locator: ChannelB-DIMM1
	Bank Locator: BANK 3
	Type: Unknown
	Type Detail: None
	Speed: Unknown
	Manufacturer: [Empty]
	Serial Number: [Empty]
	Asset Tag: 9876543210
	Part Number: [Empty]
	Rank: Unknown
	Configured Memory Speed: Unknown

