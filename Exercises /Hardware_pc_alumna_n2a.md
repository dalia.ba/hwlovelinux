# Extract hardware info from host with linux

## system

### system model, bios version and bios date, how old is the hardware?
Para saber cual es la version de la bios hay que poner en la terminal el comando **dmidecode -s bios-version**
```
[root@a29 ~]# dmidecode -s bios-version
F5
```
También para saber el la bios date hay que usar el comando **dmidecode -s bios-release-date**
```
[root@a29 ~]# dmidecode -s bios-release-date
01/17/2014
```
Para saber el system model hay que usar el comando **dmidecode -s system-product-name**
```
[root@a29 ~]# dmidecode -s system-product-name 
H81M-S2PV
```
## mainboard model, link to manual, link to product
Para saber el mainboard hay que poner en el terminal **dmidecode -s baseboard-product-name**
```
[root@a29 ~]# dmidecode -s baseboard-product-name
H81M-S2PV
```
Link to manual: https://download.gigabyte.com/FileList/Manual/mb_manual_ga-h81m-s2pv_e.pdf
Link to product: http://es.gigabyte.com/products/page/mb/ga-h81m-s2pvrev_10#kf
### memory banks (free or occupied)
Para saber la información de la memoria hay que poner en el terminal **dmidecode -t memory**
```
[root@a29 ~]# dmidecode -t memory
# dmidecode 3.2
Getting SMBIOS data from sysfs.
SMBIOS 2.7 present.

Handle 0x0007, DMI type 16, 23 bytes
Physical Memory Array
	Location: System Board Or Motherboard
	Use: System Memory
	Error Correction Type: None
	Maximum Capacity: 32 GB
	Error Information Handle: Not Provided
	Number Of Devices: 2

Handle 0x0041, DMI type 17, 34 bytes
Memory Device
	Array Handle: 0x0007
	Error Information Handle: Not Provided
	Total Width: 64 bits
	Data Width: 64 bits
	Size: 4 GB
	Form Factor: DIMM
	Set: None
	Locator: ChannelA-DIMM0
	Bank Locator: BANK 0
	Type: DDR3
	Type Detail: Synchronous
	Speed: 1400 MT/s
	Manufacturer: Kingston
	Serial Number: 44567419
	Asset Tag: 9876543210
	Part Number: 9905584-014.A00LF 
	Rank: 1
	Configured Memory Speed: 1400 MT/s

Handle 0x0043, DMI type 17, 34 bytes
Memory Device
	Array Handle: 0x0007
	Error Information Handle: Not Provided
	Total Width: Unknown
	Data Width: Unknown
	Size: No Module Installed
	Form Factor: DIMM
	Set: None
	Locator: ChannelA-DIMM1
	Bank Locator: BANK 1
	Type: Unknown
	Type Detail: None
	Speed: Unknown
	Manufacturer: [Empty]
	Serial Number: [Empty]
	Asset Tag: 9876543210
	Part Number: [Empty]
	Rank: Unknown
	Configured Memory Speed: Unknown

Handle 0x0044, DMI type 17, 34 bytes
Memory Device
	Array Handle: 0x0007
	Error Information Handle: Not Provided
	Total Width: 64 bits
	Data Width: 64 bits
	Size: 4 GB
	Form Factor: DIMM
	Set: None
	Locator: ChannelB-DIMM0
	Bank Locator: BANK 2
	Type: DDR3
	Type Detail: Synchronous
	Speed: 1400 MT/s
	Manufacturer: Kingston
	Serial Number: 55561522
	Asset Tag: 9876543210
	Part Number: 9905584-014.A00LF 
	Rank: 1
	Configured Memory Speed: 1400 MT/s

Handle 0x0046, DMI type 17, 34 bytes
Memory Device
	Array Handle: 0x0007
	Error Information Handle: Not Provided
	Total Width: Unknown
	Data Width: Unknown
	Size: No Module Installed
	Form Factor: DIMM
	Set: None
	Locator: ChannelB-DIMM1
	Bank Locator: BANK 3
	Type: Unknown
	Type Detail: None
	Speed: Unknown
	Manufacturer: [Empty]
	Serial Number: [Empty]
	Asset Tag: 9876543210
	Part Number: [Empty]
	Rank: Unknown
	Configured Memory Speed: Unknown
```
### how many disks and types can be connected

```

```
### chipset, link to 

## cpu

### cpu model, year, cores, threads, cache 
Para saber la información de la cpu hay que poner en el terminal el comando **lscpu**
```
[root@a29 ~]# lscpu
Architecture:                    x86_64
CPU op-mode(s):                  32-bit, 64-bit
Byte Order:                      Little Endian
Address sizes:                   39 bits physical, 48 bits virtual
CPU(s):                          2
On-line CPU(s) list:             0,1
Thread(s) per core:              1
Core(s) per socket:              2
Socket(s):                       1
NUMA node(s):                    1
Vendor ID:                       GenuineIntel
CPU family:                      6
Model:                           60
Model name:                      Intel(R) Pentium(R) CPU G3250 @ 3.20GHz
Stepping:                        3
CPU MHz:                         1193.901
CPU max MHz:                     3200.0000
CPU min MHz:                     800.0000
BogoMIPS:                        6385.48
Virtualization:                  VT-x
L1d cache:                       64 KiB
L1i cache:                       64 KiB
L2 cache:                        512 KiB
L3 cache:                        3 MiB
NUMA node0 CPU(s):               0,1
Vulnerability Itlb multihit:     KVM: Mitigation: VMX disabled
Vulnerability L1tf:              Mitigation; PTE Inversion; VMX conditional cache flushes, SMT disab
                                 led
Vulnerability Mds:               Mitigation; Clear CPU buffers; SMT disabled
Vulnerability Meltdown:          Mitigation; PTI
Vulnerability Spec store bypass: Mitigation; Speculative Store Bypass disabled via prctl and seccomp
Vulnerability Spectre v1:        Mitigation; usercopy/swapgs barriers and __user pointer sanitizatio
                                 n
Vulnerability Spectre v2:        Mitigation; Full generic retpoline, IBPB conditional, IBRS_FW, STIB
                                 P disabled, RSB filling
Vulnerability Srbds:             Mitigation; Microcode
Vulnerability Tsx async abort:   Not affected
Flags:                           fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat p
                                 se36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdp
                                 e1gb rdtscp lm constant_tsc arch_perfmon pebs bts rep_good nopl xto
                                 pology nonstop_tsc cpuid aperfmperf pni pclmulqdq dtes64 monitor ds
                                 _cpl vmx est tm2 ssse3 sdbg cx16 xtpr pdcm pcid sse4_1 sse4_2 movbe
                                  popcnt tsc_deadline_timer xsave rdrand lahf_lm abm cpuid_fault inv
                                 pcid_single pti ssbd ibrs ibpb stibp tpr_shadow vnmi flexpriority e
                                 pt vpid ept_ad fsgsbase tsc_adjust erms invpcid xsaveopt dtherm ara
                                 t pln pts md_clear flush_l1d
```
### socket 

## pci

### number of pci slots, lanes available
 ```
 [root@a29 ~]# lspci
00:00.0 Host bridge: Intel Corporation 4th Gen Core Processor DRAM Controller (rev 06)
00:02.0 VGA compatible controller: Intel Corporation Xeon E3-1200 v3/4th Gen Core Processor Integrated Graphics Controller (rev 06)
00:03.0 Audio device: Intel Corporation Xeon E3-1200 v3/4th Gen Core Processor HD Audio Controller (rev 06)
00:14.0 USB controller: Intel Corporation 8 Series/C220 Series Chipset Family USB xHCI (rev 05)
00:16.0 Communication controller: Intel Corporation 8 Series/C220 Series Chipset Family MEI Controller #1 (rev 04)
00:1a.0 USB controller: Intel Corporation 8 Series/C220 Series Chipset Family USB EHCI #2 (rev 05)
00:1b.0 Audio device: Intel Corporation 8 Series/C220 Series Chipset High Definition Audio Controller (rev 05)
00:1c.0 PCI bridge: Intel Corporation 8 Series/C220 Series Chipset Family PCI Express Root Port #1 (rev d5)
00:1c.2 PCI bridge: Intel Corporation 8 Series/C220 Series Chipset Family PCI Express Root Port #3 (rev d5)
00:1c.3 PCI bridge: Intel Corporation 82801 PCI Bridge (rev d5)
00:1d.0 USB controller: Intel Corporation 8 Series/C220 Series Chipset Family USB EHCI #1 (rev 05)
00:1f.0 ISA bridge: Intel Corporation H81 Express LPC Controller (rev 05)
00:1f.2 SATA controller: Intel Corporation 8 Series/C220 Series Chipset Family 6-port SATA Controller 1 [AHCI mode] (rev 05)
00:1f.3 SMBus: Intel Corporation 8 Series/C220 Series Chipset Family SMBus Controller (rev 05)
02:00.0 Ethernet controller: Realtek Semiconductor Co., Ltd. RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller (rev 06)
03:00.0 PCI bridge: Intel Corporation 82801 PCI Bridge (rev 41)
 ```
### devices connected
```
[root@a29 ~]# lsblk
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda      8:0    0 465.8G  0 disk 
├─sda1   8:1    0     1K  0 part 
├─sda5   8:5    0   100G  0 part /
├─sda6   8:6    0    95G  0 part 
└─sda7   8:7    0    10G  0 part [SWAP]
```
### network device, model, kernel module, speed

### audio device, model, kernel module

### vga device, model, kernel module

## hard disks

### /dev/* , model, bus type, bus speed

### test fio random (IOPS) and sequential (MBps)
